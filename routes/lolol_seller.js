var express = require('express');
var router = express.Router();
const passport = require('passport');
const SellerController 	= require('./../controller/v1/SellerController');

AuthMiddleware = function(req, res, next) {
    require('./../middleware/passport').consumerAuth(passport);
    passport.authenticate('jwt', function(err, user, info) {
        
        if(!user)
        {   
            console.log(req.body);
            console.log(`Authorization::${req.headers.authorization}`);
            if(info)
            {
                console.log(`error_message::${info.message}`);
                return ReE(res,info.message,401);
            }  
            console.log(`error_message::${err}`);  
            return ReE(res,err,401);
                
        }
        req.user = user;
        return next();
  
    })(req, res, next);
  
};


router.post('/createSeller',SellerController.createSeller); 

router.post('/addProduct',SellerController.addProduct); 

module.exports = router;