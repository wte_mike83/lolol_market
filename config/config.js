require('dotenv').config();//instatiate environment variables

CONFIG = {} //Make this global to use all over the application

CONFIG.app          = process.env.APP   || 'dev';
CONFIG.port         = process.env.PORT  || '3000';

CONFIG.db_dialect   = process.env.DB_DIALECT    || 'mysql';
CONFIG.db_host      = process.env.DB_HOST       || 'localhost';
CONFIG.db_port      = process.env.DB_PORT       || '3306';
CONFIG.db_name      = process.env.DB_NAME       || 'name';
CONFIG.db_user      = process.env.DB_USER       || 'root';
CONFIG.db_password  = process.env.DB_PASSWORD   || '';

CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION || 'jwt_please_change';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '10000';
CONFIG.jwt_encryption_admin  = process.env.JWT_ENCRYPTION_ADMIN || 'jwt_please_change';

CONFIG.trioApi  = process.env.trioApi ;
// CONFIG.filePath  = `http://${process.env.Host}:${process.env.ImagePort}/images/` ||'http://192.168.88.163:3007/images/' ;
CONFIG.filePath  = process.env.FILE_PATH || '';
CONFIG.storePath  = process.env.UPLOAD_IMAGE_PATH || 'public/images';
CONFIG.host = process.env.Host ||'http://192.168.88.163:3005' ;
// CONFIG.redisPort  = process.env.redisPort||6379 ;
// CONFIG.redisUrl = process.env.redisUrl||'localhost' ;
// CONFIG.redisCacheTime = process.env.redisCacheTime||30 ;



CONFIG.db_dialect_main   = process.env.DB_DIALECT_MAIN    || 'mysql';
CONFIG.db_host_main       = process.env.DB_HOST_MAIN        || 'localhost';
CONFIG.db_port_main       = process.env.DB_PORT_MAIN        || '3306';
CONFIG.db_name_main       = process.env.DB_NAME_MAIN        || 'name';
CONFIG.db_user_main       = process.env.DB_USER_MAIN        || 'root';
CONFIG.db_password_main   = process.env.DB_PASSWORD_MAIN    || '';