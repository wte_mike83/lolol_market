const moment = require('moment');
'use strict';


module.exports = (sequelize, DataTypes) => {

  var MarketShop = sequelize.define('MarketShop', {
   
    
    shop_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    consumer_id: DataTypes.INTEGER,
    shop_name: DataTypes.STRING,
    shop_type: DataTypes.STRING,
    shop_address1: DataTypes.STRING,
    shop_address2: DataTypes.STRING,
    shop_country: DataTypes.TEXT,
    shop_state: DataTypes.STRING,
    shop_city: DataTypes.STRING,
    shop_postcode: DataTypes.STRING,
    shop_latitude: DataTypes.DECIMAL,
    shop_longitude: DataTypes.DECIMAL,
    contact_country_code: DataTypes.STRING,
    shop_contact_no: DataTypes.STRING,
    verify: DataTypes.TINYINT,
    acc_active: DataTypes.TINYINT,
    charge_rate: DataTypes.DECIMAL,
    createdAt:'TIMESTAMP',
  
  }, {
    freezeTableName: true,
    tableName: 'market_shop',
    timestamps: true, 
    getterMethods: {

      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      }
    } 
  });



  return MarketShop;
};