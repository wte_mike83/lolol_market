const moment = require('moment');
'use strict';


module.exports = (sequelize, DataTypes) => {

  var MarketProduct = sequelize.define('MarketProduct', {
   
    
    product_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    shop_id: DataTypes.INTEGER,
    product_pic: DataTypes.STRING,
    product_name: DataTypes.STRING,
    product_desc: DataTypes.STRING,
    shop_country: DataTypes.TEXT,
    stock: DataTypes.STRING,
    category_id: DataTypes.INTEGER,
    weight: DataTypes.DECIMAL,
    shipping_id: DataTypes.INTEGER,
    condition: DataTypes.STRING,
    self_shipping: DataTypes.TINYINT,
    publish: DataTypes.TINYINT,
    createdAt:'TIMESTAMP',
  
  }, {
    freezeTableName: true,
    tableName: 'market_product',
    timestamps: true, 
    getterMethods: {

      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      }
    } 
  });



  return MarketProduct;
};