'use strict';
module.exports = (sequelize, DataTypes) => {

  var RiderVerifyProofOld = sequelize.define('RiderVerifyProofOld', {
   
    proof_id:{type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    rider_id: DataTypes.INTEGER,
    file_name: DataTypes.STRING,
    file_type: DataTypes.STRING,
    file_size: DataTypes.STRING,
    file_display_name: DataTypes.STRING,
    file_checksum: DataTypes.STRING,    
    proof_type: DataTypes.ENUM('profile', 'selfieIC', 'front_license', 'back_license', 'vehicle_registration', 'insurance'),
    proof_doc: DataTypes.STRING,
    status: DataTypes.STRING,
    reason: DataTypes.STRING,
    
    
  }, {
    freezeTableName: true,
    tableName: 'rider_verify_proof',

  });

  RiderVerifyProofOld.associate = function(models) {
    this.rider_id = this.belongsTo(models.Rider, {foreignKey: 'rider_id'});
  };


  return RiderVerifyProofOld;
};