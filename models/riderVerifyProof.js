'use strict';
module.exports = (sequelize, DataTypes) => {

  var RiderVerifyProof = sequelize.define('RiderVerifyProof', {
   
    proof_id:{type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    rider_id: DataTypes.INTEGER,
    files: DataTypes.STRING, 
    proof_type: DataTypes.ENUM('profile', 'selfieIC', 'front_license', 'back_license', 'vehicle_registration', 'insurance'),
    status: DataTypes.STRING,
    reason: DataTypes.STRING,
    
    
  }, {
    freezeTableName: true,
    tableName: 'rider_verify_proof',

  });

  RiderVerifyProof.associate = function(models) {
    this.rider_id = this.belongsTo(models.Rider, {foreignKey: 'rider_id'});
  };


  return RiderVerifyProof;
};