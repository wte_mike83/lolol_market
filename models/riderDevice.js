'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var RiderDevice = sequelize.define('RiderDevice', {

    r_device_id:{type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    app_token:DataTypes.STRING,
    uuid:DataTypes.STRING,
    rider_id:DataTypes.INTEGER,

  }, {
    freezeTableName: true,
    tableName: 'rider_device',
    timestamps: false

  });

  RiderDevice.associate = function(models) {
    
    // associations can be defined here
  };


  RiderDevice.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  RiderDevice.prototype.getJWT = function () {
    let expiration_time = parseInt(28800);
    return "Bearer "+jwt.sign({r_device_id:this.r_device_id}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
  };



  return RiderDevice;
};