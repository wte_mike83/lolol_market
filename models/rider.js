const moment = require('moment');
'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var Rider = sequelize.define('Rider', {
   
    
    rider_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    email: DataTypes.STRING,
    country_code: DataTypes.STRING,
    phone_no: DataTypes.STRING,
    full_name: DataTypes.STRING,
    password: DataTypes.STRING,
    address: DataTypes.TEXT,
    country: DataTypes.STRING,
    ic_no: DataTypes.STRING,
    dl_no: DataTypes.STRING,
    dl_exp_date: DataTypes.DATEONLY,
    vehicle_model: DataTypes.STRING,
    vehicle_plate_no: DataTypes.STRING,
    roadtax_exp_date: DataTypes.DATEONLY,
    service_area: DataTypes.STRING,
    acc_holder_name: DataTypes.STRING,
    bank_name: DataTypes.STRING,
    bank_acc_no: DataTypes.STRING,

    acc_active: DataTypes.TINYINT,
    verify: DataTypes.TINYINT,
    online_status: DataTypes.TINYINT,
    email_verification_flag: DataTypes.INTEGER,
    email_verification_code: DataTypes.STRING,
    point_collected: DataTypes.INTEGER,
    wallet: DataTypes.DECIMAL,
    wallet_flag: DataTypes.TINYINT,
    wallet_pin: DataTypes.STRING,
    referral_code: DataTypes.STRING,
    refer_from: DataTypes.STRING,
    refer_date: 'TIMESTAMP',
    notification: DataTypes.STRING,
    notif_bell: 'TIMESTAMP',
    profile_pic: DataTypes.STRING,
    profile_thumbnail: DataTypes.STRING,
    createdAt:'TIMESTAMP',
    profile_checksum: DataTypes.STRING,
    longitude:DataTypes.DECIMAL,
    latitude:DataTypes.DECIMAL,
  
  }, {
    freezeTableName: true,
    tableName: 'rider',
    timestamps: true, 
    getterMethods: {
      profile_pic_url:  function() {

        if(this.getDataValue('profile_pic') != null)
          return `${CONFIG.filePath}rider/${this.getDataValue('rider_id')}/${this.getDataValue('profile_pic')}`; //"http://129.126.133.186:4001/rider/9/05qedsbdr7m5jtp.jpg"
        else if(this.getDataValue('profile_pic') === undefined)
          return ;
        else
          return CONFIG.filePath +'media/default-user.png'; //http://129.126.133.186:4001/media/default-user.png"
      },
      profile_thumbnail_url:  function() {

        if(this.getDataValue('profile_thumbnail') != null)
          return `${CONFIG.filePath}rider/${this.getDataValue('rider_id')}/${this.getDataValue('profile_thumbnail')}`; //"http://129.126.133.186:4001/rider/9/05qedsbdr7m5jtp_thumb.jpg"
        else if(this.getDataValue('profile_thumbnail') === undefined)
          return ;
        else
          return null;
      },
      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
      }
    } 
  });

  Rider.associate = function(models) {
    this.rider_id = this.hasMany(models.RiderVerifyProof, {foreignKey: 'rider_id'});
  };

  Rider.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    delete json['password'];

    return json;
  };

  Rider.prototype.getJWT = function (r_device_id) {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer "+jwt.sign({rider_id:this.rider_id,r_device_id:r_device_id}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
  };

  return Rider;
};