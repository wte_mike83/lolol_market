const MarketShop   = require('../../models').MarketShop;
const MarketProduct   = require('../../models').MarketProduct;

const State = require('../../models').State;
const District = require('../../models').District;
const dir = 'C:/project/lolol_seller/controller/v1/public';

const md5File = require('md5-file');
const sharp = require('sharp');


const test = async function (req, res) {
    console.log("hihihihi")

}
module.exports.test = test;




const createSeller = async function (req, res) {

    //const randomNumber = require("random-number-csprng");

    const body = Keyfilter(req.body, ['shop_name', 'shop_type', 'shop_address1', 'shop_address2', 'shop_country', 'shop_state', 'shop_city', 'shop_postcode', 'shop_latitude', 'shop_longitude', 'contact_country_code', 'shop_contact_no']);

    const seller = req.user;



    validation = new Promise((resolve, reject) => {
        req.checkBody({
            'restaurant_name': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 200
                    },
                    errorMessage: 'Restaurant Name cannot more than 200 character.'
                },
                errorMessage: 'Empty Restaurant Name.'
            },
            'restaurant_address1': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 250
                    },
                    errorMessage: 'Restaurant Address 1 cannot more than 250 character.'
                },
                errorMessage: 'Empty Restaurant Address 1.'
            },
            'restaurant_address2': {
                optional: true,
                isLength: {
                    options: {
                        max: 200
                    },
                    errorMessage: 'Restaurant Address 2 cannot more than 250 character.'
                }
            },
            'restaurant_city': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 50
                    },
                    errorMessage: 'Restaurant City cannot more than 50 character.'
                },
                errorMessage: 'Empty Restaurant City.'
            },
            'restaurant_state': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 50
                    },
                    errorMessage: 'Restaurant State cannot more than 50 character.'
                },
                errorMessage: 'Empty Restaurant State.'
            },
            'restaurant_country': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 50
                    },
                    errorMessage: 'Restaurant Country cannot more than 50 character.'
                },
                isIn: {
                    options: [
                        ['Malaysia']
                    ],
                    errorMessage: 'Invalid Restaurant Country',
                },
                errorMessage: 'Empty Restaurant Country.'
            },
            'restaurant_postcode': {
                notEmpty: true,
                isLength: {
                    options: {
                        max: 30
                    },
                    errorMessage: 'Restaurant Postcode cannot more than 30 character.'
                },
                errorMessage: 'Empty Restaurant Postcode.'
            },
            'restaurant_latitude': {
                notEmpty: true,
                errorMessage: 'Empty Restaurant Latitude.'
            },
            'restaurant_longitude': {
                notEmpty: true,
                errorMessage: 'Empty Restaurant Longitude.'
            },

            'restaurant_type': {
                notEmpty: true,
                isIn: {
                    options: [
                        ['freelancer', 'company']
                    ],
                    errorMessage: 'Invalid Restaurant Country',
                },
                errorMessage: 'Empty Restaurant Type'
            }

        });

        var errors = req.validationErrors();

        console.log("restaurant_state" + body.restaurant_state)
        console.log("restaurant_city" + body.restaurant_city)
        console.log("restaurant_postcode" + body.restaurant_postcode)
        console.log("body.field" + body.field)

        checkStateDistrist = new Promise((resolve, reject) => {

            if (!errors)
                errors = [];

            if (body.restaurant_state && body.restaurant_city && body.restaurant_postcode) {
                State.findOne({
                    attributes: ['state_id', 'state_name'],
                    where: {
                        state_name: body.restaurant_state
                    },
                    include: [{
                        model: District,
                        attributes: ['state_id', 'sub_district', 'postcode'],
                        where: {
                            sub_district: body.restaurant_city
                        },
                        required: false
                    }]
                }).then(state => {

                    if (state == null) {
                        errors.push({
                            location: "body",
                            param: "restaurant_state",
                            msg: "Invalid Restaurant State",
                            value: body.restaurant_state
                        })
                        resolve();
                        //reject("Invalid Restaurant State Or City");

                    } else if (state.Districts == null || state.Districts.length == 0) {

                        errors.push({
                            location: "body",
                            param: "restaurant_city",
                            msg: "Invalid Restaurant City",
                            value: body.restaurant_city
                        })
                        resolve();
                    } else {

                        let postcode = JSON.parse(state.Districts[0].postcode);

                        if (!postcode.includes(parseInt(body.restaurant_postcode))) {
                            errors.push({
                                location: "body",
                                param: "restaurant_postcode",
                                msg: "Invalid Postcode",
                                value: body.restaurant_postcode
                            })
                        }
                        resolve();
                    }
                }).catch(err => {

                    errors.push({
                        location: "body",
                        param: "restaurant_state",
                        msg: "Error. Invalid Restaurant State",
                        value: body.restaurant_state
                    })

                    resolve();
                })
            } else
                resolve();

        })

        checkStateDistrist.then(ret => {
            if (errors && errors.length > 0)
                reject(errors);
            else
                resolve(ret);
        })

    })

    validation.then(data => {
        console.log(data)
        // return Restaurant.findAndCountAll({
        //         where: {
        //             merchant_id: merchant.merchant_id
        //         }
        //     }).then(total => {

        //         generate = size => {
        //             return new Promise((resolve, reject) => {
        //                 var code = [];

        //                 var splitter = 2;
        //                 var divider = Math.floor(size / splitter);
        //                 while (divider > 9) {
        //                     splitter++;
        //                     divider = Math.floor(size / splitter);
        //                 }

        //                 var min_num = Math.pow(10, divider - 1);
        //                 var max_num = Math.pow(10, divider) - 1;

        //                 // console.log({ divider, splitter, min_num, max_num });

        //                 var i = 0;
        //                 while (i < splitter) {
        //                     code[i] = randomNumber(min_num, max_num);
        //                     i++;
        //                 }

        //                 var reminder = size % divider;
        //                 if (reminder) {
        //                     var reminder_min = Math.pow(10, reminder - 1);
        //                     var reminder_max = Math.pow(10, reminder) - 1;
        //                     code[i] = randomNumber(reminder_min, reminder_max);

        //                     // console.log({ reminder, reminder_min, reminder_max });
        //                 }

        //                 Promise.all(code)
        //                     .then(data => {

        //                         resolve(41 + data.join(""));

        //                     })
        //                     .catch(err => reject(err));
        //             });
        //         };

        //         generate(16).then(code => {
        //             body.payment_code = code;

        //             body.headquarters = (total.count > 0) ? 0 : 1;
        //             body.merchant_id = (merchant.main_merchant) ? merchant.main_merchant : merchant.merchant_id;
        //             body.isVerified = 1;
        //             body.food_preference = '{"Pork":0,"Beef":0,"Seafood":0,"Chicken":0,"Lamb":0,"Other":0}';

        //             body.restaurant_desc = (body.restaurant_desc) ? body.restaurant_desc : "";
        //             body.restaurant_address2 = (body.restaurant_address2) ? body.restaurant_address2 : "";

        //             body.restaurant_business_phone = (body.restaurant_business_phone) ? body.restaurant_business_phone : null;
        //             body.business_phone_country_code = (body.business_phone_country_code) ? body.business_phone_country_code : null;

        //             body.restaurant_fax = (body.restaurant_fax) ? body.restaurant_fax : null;
        //             body.fax_country_code = (body.fax_country_code) ? body.fax_country_code : null;
        //             body.restaurant_website = (body.restaurant_website) ? body.restaurant_website : null;

        //             body.category = (body.category) ? body.category : null;
        //             body.ambience = (body.ambience) ? body.ambience : null;
        //             body.amenity = (body.amenity) ? body.amenity : null;


        //             return Restaurant.create(body).then(rest => {
        //                     let permission = {
        //                         "manage_branch_profile": true,
        //                         "manage_user": true,
        //                         "manage_wallet": true,
        //                         "manage_product": true,
        //                         "product_availability": true,
        //                         "manage_order": true,
        //                         "take_order": true,
        //                         "table_calling": true,
        //                         "manage_table": true,
        //                         "manage_review": true,
        //                         "manage_report": true,
        //                         "manage_offer": true,
        //                         "manage_payment": true,
        //                         "manage_refund": true,
        //                         "manage_kot": true,
        //                         "manage_topup": false
        //                     };

        //                     let m = [merchant.merchant_id]
        //                     if (merchant.main_merchant)
        //                         m.push(merchant.main_merchant)

        //                     return Merchant.update({
        //                         permission: Sequelize.literal("CASE WHEN permission IS NULL THEN '\{\"" + rest.restaurant_id + "\":" + JSON.stringify(permission) + "}\' ELSE REGEXP_REPLACE(permission, '}$', ',\"" + rest.restaurant_id + "\":" + JSON.stringify(permission) + "}') END")
        //                     }, {
        //                         where: {
        //                             merchant_id: {
        //                                 [Sequelize.Op.in]: m
        //                             }
        //                         }
        //                     }).then(updsta => {

        //                         return ReS(res, 'Restaurant Created Successfully.', {
        //                             data: rest.restaurant_id
        //                         }, 200);
        //                     }).catch(err => {
        //                         return ReE(res, err, 400);
        //                     })
        //                 }),
        //                 function (err) {
        //                     return ReE(res, err, 400);
        //                 }

        //         }).catch(err => {
        //             return ReE(res, err, 400);
        //         })

        //     }),
        //     function (err) {
        //         return ReE(res, err, 400);
        //     }


        return ReS(res, 'Restaurant Created Successfully.', {
            data: data
        }, 200)

    }).catch((error) => {
        return ReE(res, error, 400);
    })

}
module.exports.createSeller = createSeller;



const addProduct = async function (req, res) {

    //const seller = req.user;
    var seller = {}
    seller.consumer_id = 3308;

    const maxSize = 50 * 1024 * 1024;

    const multer = require('multer');
    const helper = require("./helper");

    const dest = dir + '/product/1';

    var storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, dest)
        },
        filename: (req, file, cb) => {
            cb(null, generateRandomKey(5) + generateRandomKey(5) + generateRandomKey(5) + '.' + file.originalname.split('.').pop())
        }
    });



    var upload = multer({
        storage: storage,
        limits: {
            fileSize: maxSize,
            files: 9
        },

        fileFilter: function (req, file, cb) {
            if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'application/octet-stream') {
                return cb(new Error('Only image file are allowed'))
            }
            cb(null, true);
        }
    }).array('image');



    createImageFolder = new Promise((resolve, reject) => {

        helper.checkDirectorySync(dir).then(ret => {

            helper.checkDirectorySync(dest).then(ret => {

                resolve(ret);

            }).catch((err) => {
                reject(err);
            })

        }).catch((err) => {
            reject(err);
        })
    });



    createImageFolder.then((directory) => {

        return upload(req, res, function (err) {
            let files = req.files;

            if (err) {

                return ReE(res, err, 400); // return error

            } else if (err) {

                return ReE(res, err, 400); // return error

            } else {


                if (files.length > 0) {
                    let images = [];
                    var promise = [];

                    convertImage = new Promise((resolve, reject) => {


                        for (i = 0; i < files.length; i++) {


                            function formImage() {

                                return new Promise((resolve, reject) => {

                                    var obj = files[i];
                                    let image = {};

                                    //console.log("loop " + JSON.stringify(obj))

                                    image.review_img = obj.filename;
                                    image.checkSum = md5File.sync(obj.destination + "/" + obj.filename);
                                    image.thumbnail = obj.filename.split('.')[0] + '_thumb.' + obj.originalname.split('.').pop(); //obj.filename.split('.').pop();
                                    image.thumbnail_square = obj.filename.split('.')[0] + '_thumb_square.' + obj.originalname.split('.').pop();

                                    image.destination = obj.destination;
                                    image.file = obj.path;

                                    var thumbnail_setting = [{
                                        width: 800,
                                        heigth: 400,
                                        toFile: image.thumbnail,
                                        thumbnail: "thumbnail_path"
                                    }, {
                                        width: 400,
                                        heigth: 400,
                                        toFile: image.thumbnail_square,
                                        thumbnail: "thumbnail_square"
                                    }];

                                    sharp(image.file)
                                        .metadata()
                                        .then(function (metadata) {


                                            var settings = []
                                            thumbnail_setting.map(function (setting) {
                                                if (metadata.width < setting.width) setting.width = metadata.width
                                                if (metadata.height < setting.height) setting.height = metadata.height
                                                setting.destination = image.destination
                                                setting.target = image.file
                                                settings.push(setting)

                                            })
                                            return settings;
                                        })
                                        .then(function (data) {

                                            Promise
                                                .all(data.map(d => resize(d.width, d.height, d.toFile, d.destination, d.target)))
                                                .then(result => {

                                                    images.push(image);

                                                    resolve();
                                                }).catch(err => {
                                                    console.log(err);
                                                    image.thumbnail_path = null;
                                                    image.thumbnail_square = null;
                                                    images.push(image);
                                                    resolve();
                                                })
                                        });



                                })

                            }

                            promise.push(formImage());
                        }



                        return Promise.all(promise).then(ret => {
                            resolve(images);
                        }).catch(err => {
                            reject(err)
                        })



                    });
                } else
                    return ReE(res, "no uploaded images", 400);




                convertImage.then((images) => {
                   // console.log("get images" + JSON.stringify(images))


                    req.checkBody({
                        'shop_id': {
                            notEmpty: true,
                            errorMessage: 'Shop ID is required',
                            isInt: {
                                errorMessage: 'Invalid shop ID'
                            },
                        },
                            'product_name':{
                                notEmpty: true,
                                errorMessage: 'Product name is required',
                                isLength:{
                                    options:{min:1,max:80},
                                    errorMessage:'Maximum 80 characters!'
                                }
                            },
                            'product_desc':{
                                notEmpty: true,
                                errorMessage: 'Product description is required',
                                isLength:{
                                    options:{min:1,max:5000},
                                    errorMessage:'Maximum 5000 characters!'
                                }
                            },
                        'stock':{
                            notEmpty: true,
                            isJSON:{
                                        errorMessage:'Stock require json format'
                                    },
                            errorMessage: 'Stock information is required',
                        },
                        'weight':{
                            notEmpty: true,
                            errorMessage: 'Weight is required',
                            isFloat:{
                                options:{ min: 0.00, max: 100.00 },
                                errorMessage:'Maximum 100 kg and not negative kg'
                            }
                        },
                        'catogary_id':{
                            notEmpty: true,
                            isInt:{
                                errorMessage: 'Invalid catogory ID'
                            },
                            errorMessage: 'Catogory ID is required',
                        },
                        'shipping_id':{
                            notEmpty: true,
                            isInt:{
                                errorMessage: 'Invalid Shipping ID'
                            },
                            errorMessage: 'Shipping ID is required',
                        },
                        'condition':{
                            notEmpty: true,
                            isIn:{
                                options: [['new', 'used']],
                                errorMessage: 'Only new or used',

                            },
                            errorMessage: 'Condition is required',
                        },
                        'self_shipping': {
                            notEmpty: true,
                            isIn: {
                                options: [
                                    ['0', '1']
                                ],
                                errorMessage: 'Only 0 or 1',

                            }
                        },
                        'publish': {
                            notEmpty: true,
                            isIn: {
                                options: [
                                    ['0', '1']
                                ],
                                errorMessage: 'Only 0 or 1',

                            },
                            errorMessage: 'Publish states is required',
                        }

                    });

                    var errors = req.validationErrors(); // get validation error 

                    if (errors) {
                        return ReE(res, errors, 400); // return error
                    }

                    const body = Keyfilter(req.body, ['shop_id', 'product_name', 'product_desc', 'stock', 'weight', 'catogary_id', 'shipping_id', 'condition', 'self_shipping', 'publish']);

                    console.log("key filer"+JSON.stringify(body))


                    MarketShop.findOne({ attributes: ['shop_id'],where:{shop_id:body.shop_id}}).then(shop =>{ 

                        console.log(" shop"+JSON.stringify(shop))
                        body.product_pic =JSON.stringify(images);

                        if(shop)
                        return MarketProduct.create(body).then(product => {
                            return ReS(res, 'Successfully created new product',{data:product}, 200);
                        }), function(err) {
                            return ReE(res, err, 400);
                        }
                       
                    });



                });

            }


        })
    })



}
module.exports.addProduct = addProduct;