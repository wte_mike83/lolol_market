

const NotificationModel   = require('../../models/notification');
const Device          = require('../../models').Device;
const MerchantDevice          = require('../../models').MerchantDevice;
const moment = require('moment'); 


    /**
    * handle Notification 
    *
    * @param  int  notification Store Up Notification Flag
    * @param  int  userID User/Merchant/Rider ID
    * @param  int  typeID Order/Review ID
    * @param  String  userType User (rider|client|merchant)
    * @param  String  NotifType Notification Type (order|review)
    * @param  String  msg Notification Message
    * @param  String  orderStatus Order Status
    * @param  String  action Notifiaction Action 
    * @param  int  data Param Flag
    
    * @return result
    */
var handleNotification = function(notification, userID, typeID, userType, NotifType, msg, orderStatus, action, data = true, tag = null, dataParam = null){
    
    var fs = require('fs');
    var file = 'logs/notification-'+moment().format('YYYY-MM-DD')+'.txt';

    return new Promise((resolve, reject) => {
        var param = {};
        insertNotification = new Promise((resolve, reject) => {
            
            if(notification){
                param.message = msg;
               
                if(userType === "rider" || userType === "consumer" || userType === "merchant"){
    
                    param.user_type = userType;
                    param.user_id = userID;
    
                    if(tag != null && tag != "")
                        param.tag = tag;
                    
                    if(NotifType === "order" || NotifType === "review" || NotifType === "reserve" || NotifType === "payment"){
                        param.notification_type = NotifType;
                        param.type_id = typeID;
                        
                        /*param.createdAt = moment().utcOffset("+08:00").format('YYYY-MM-DDTHH:mm:ssZ');
                        param.createdAt = '2018-05-05T12:12:12.000+08:00';
                        console.log(param.createdAt)
                        console.log(moment().format('YYYY-MM-DDTHH:mm:ss.000Z'))
                        console.log(moment.utc().toDate())
                        console.log(moment.utc().toDate())*/

                        // Create an instance of model SomeModel
                        var noti = new NotificationModel(param);
                        // console.log(111)
                        // console.log(param)
                        noti.save().then(result => {
                            // console.log(222)
                            // console.log(result)
                            if(result == null)
                                reject("Failed to insert Notification!");
                            else
                                resolve(result._id)
                        }).catch(err =>{
                            reject(err);
                        })
                    }else{
                        reject("Invalid Notification Type")
                    }
    
                }else
                    reject("Invalid User");
            }else{
                resolve(null);
            }

        })

        insertNotification.then(result =>{

            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification Start. '+userType+':'+userID+'\r\n');
            
            param = {};

            if(data)
            {     
                if(dataParam != null)
                {
                    try{
                        param = JSON.parse(dataParam);
                    }catch(err){
                        param = {};
                    }
                }

                param.type = NotifType;
                switch (NotifType){
                    case "order":
                        param.order_id = typeID;
                        param.order_status = orderStatus;
                    break;

                    case "review":
                        param.review_id = typeID;
                    break;

                    case "reserve":
                        param.reserve_id = typeID;
                    break;

                    case "payment":
                        param.trans_id = typeID;
                    break;
                }
            
                if(result != null)
                    param.notification_id = result;
            }
            
            // if(dataParam != null)
            // {
            //     try{
            //         param = JSON.parse(dataParam);
            //     }catch(err){
            //         param = {};
            //     }
            // }
                
            // if(data)
            // {      
            //     switch (userType){
            //         case "order":
            //             param.order_id = typeID;
            //             param.order_status = orderStatus;
            //         break;

            //         case "review":
            //             param.review_id = typeID;
            //         break;

            //         case "reserve":
            //             param.reserve_id = typeID;
            //         break;
            //     }
            // }
            
            // if(result != null)
            //     param.notification_id = result;

            var msg_payload = { 'msg' : msg };


            switch (userType){
                case "rider":
                    //var where = 
                    var server_key = CONFIG.RIDER_FCM_SERVER_KEY;
                break;
                
                case "consumer":
                    var where = {consumer_id:userID}
                    var server_key = CONFIG.CLIENT_FCM_SERVER_KEY;

                    Device.findAll({attributes:['app_token','device_id'], where:where}).then(device =>{
                        fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Device Count: '+device.length +'\r\n');
        
                        if(device.length > 0){
                            var promises = [];
        
                            var FCM = require('./lib/fcm');
                
                            var fcm = new FCM(server_key);
        
                            device.map(function(v) {
                                
                                function fcmPush(v){
                                    return new Promise((resolve, reject) => {
                                        fcmPushNotification(msg_payload,v.app_token,param, fcm).then(result =>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+result +'\r\n');
                                            
                                            resolve() 
                                        }).catch(err=>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+err +'\r\n');
                                            resolve()
                                        }) 
                                    })
                                }
                                promises.push(fcmPush(v));
                            })
        
                            Promise.all(promises).then(function(result) {
                                
                                fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification End.\r\n');
                                resolve(); 
                                
                            }).catch(err => {
                                reject(err);
                            })
                        }
                        
                    }).catch(err =>{
                        reject(err)
                    })
                break;
                
                case "merchant":
                    var where = {merchant_id:userID}
                    //var server_key = CONFIG.MERCHANT_FCM_SERVER_KEY;
                    var server_key = CONFIG.MERCHANT_FCM_SERVER_KEY;

                    MerchantDevice.findAll({attributes:['app_token','device_id'], where:where}).then(device =>{
                        fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Device Count: '+device.length +'\r\n');
                        
                        if(device.length > 0){
                            
                            var promises = [];
                            
                            var FCM = require('./lib/fcm');

                            var fcm = new FCM(server_key);

                            device.map(function(v) {
                                
                                function fcmPush(v){
                                    return new Promise((resolve, reject) => {
                                        
                                        fcmPushNotification(msg_payload,v.app_token,param, fcm).then(result =>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+result +'\r\n');
                                            
                                            resolve() 
                                        }).catch(err=>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+err +'\r\n');
                                            resolve()
                                        }) 
                                    })
                                }
                                promises.push(fcmPush(v));
                            })
        
                            Promise.all(promises).then(function(result) {
                                
                                fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification End.\r\n');
                                resolve(); 
                                
                            }).catch(err => {
                                reject(err);
                            })
                        }
                        
                    }).catch(err =>{
                        reject(err)
                    })
                break;
            }

            
        }).catch(err =>{
            reject(err)
        })
    });
}
exports.handleNotification = handleNotification;

var handleNotification_lolol = function(notification, userID, typeID, userType, NotifType, msg, orderStatus, action, data = true, tag = null, dataParam = null){
    
    var fs = require('fs');
    var file = 'logs/notification-'+moment().format('YYYY-MM-DD')+'.txt';

    return new Promise((resolve, reject) => {
        var param = {};
        insertNotification = new Promise((resolve, reject) => {
            
            if(notification){
                param.message = msg;
               
                if(userType === "rider" || userType === "consumer" || userType === "merchant"){
    
                    param.user_type = userType;
                    param.user_id = userID;
    
                    if(tag != null && tag != "")
                        param.tag = tag;
                    
                    if(NotifType === "order" || NotifType === "review" || NotifType === "reserve" || NotifType === "payment"){
                        param.notification_type = NotifType;
                        param.type_id = typeID;
                        
                        /*param.createdAt = moment().utcOffset("+08:00").format('YYYY-MM-DDTHH:mm:ssZ');
                        param.createdAt = '2018-05-05T12:12:12.000+08:00';
                        console.log(param.createdAt)
                        console.log(moment().format('YYYY-MM-DDTHH:mm:ss.000Z'))
                        console.log(moment.utc().toDate())
                        console.log(moment.utc().toDate())*/

                        // Create an instance of model SomeModel
                        var noti = new NotificationModel(param);
                        // console.log(111)
                        // console.log(param)
                        noti.save().then(result => {
                            // console.log(222)
                            // console.log(result)
                            if(result == null)
                                reject("Failed to insert Notification!");
                            else
                                resolve(result._id)
                        }).catch(err =>{
                            reject(err);
                        })
                    }else{
                        reject("Invalid Notification Type")
                    }
    
                }else
                    reject("Invalid User");
            }else{
                resolve(null);
            }

        })

        insertNotification.then(result =>{

            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification Start. '+userType+':'+userID+'\r\n');
            
            param = {};

            if(data)
            {     
                if(dataParam != null)
                {
                    try{
                        param = JSON.parse(dataParam);
                    }catch(err){
                        param = {};
                    }
                }

                param.type = NotifType;
                switch (NotifType){
                    case "order":
                        param.order_id = typeID;
                        param.order_status = orderStatus;
                    break;

                    case "review":
                        param.review_id = typeID;
                    break;

                    case "reserve":
                        param.reserve_id = typeID;
                    break;

                    case "payment":
                        param.trans_id = typeID;
                    break;
                }
            
                if(result != null)
                    param.notification_id = result;
            }
            
            // if(dataParam != null)
            // {
            //     try{
            //         param = JSON.parse(dataParam);
            //     }catch(err){
            //         param = {};
            //     }
            // }
                
            // if(data)
            // {      
            //     switch (userType){
            //         case "order":
            //             param.order_id = typeID;
            //             param.order_status = orderStatus;
            //         break;

            //         case "review":
            //             param.review_id = typeID;
            //         break;

            //         case "reserve":
            //             param.reserve_id = typeID;
            //         break;
            //     }
            // }
            
            // if(result != null)
            //     param.notification_id = result;

            var msg_payload = { 'msg' : msg };


            switch (userType){
                case "rider":
                    //var where = 
                    var server_key = CONFIG.RIDER_FCM_SERVER_KEY;
                break;
                
                case "consumer":
                    var where = {consumer_id:userID}
                    var server_key = CONFIG.CLIENT_FCM_SERVER_KEY;

                    Device.findAll({attributes:['app_token','device_id'], where:where}).then(device =>{
                        fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Device Count: '+device.length +'\r\n');
        
                        if(device.length > 0){
                            var promises = [];
        
                            var FCM = require('./lib/fcm');
                
                            var fcm = new FCM(server_key);
        
                            device.map(function(v) {
                                
                                function fcmPush(v){
                                    return new Promise((resolve, reject) => {
                                        fcmPushNotification(msg_payload,v.app_token,param, fcm).then(result =>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+result +'\r\n');
                                            
                                            resolve() 
                                        }).catch(err=>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+err +'\r\n');
                                            resolve()
                                        }) 
                                    })
                                }
                                promises.push(fcmPush(v));
                            })
        
                            Promise.all(promises).then(function(result) {
                                
                                fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification End.\r\n');
                                resolve(); 
                                
                            }).catch(err => {
                                reject(err);
                            })
                        }
                        
                    }).catch(err =>{
                        reject(err)
                    })
                break;
                
                case "merchant":
                    var where = {merchant_id:userID}
                    //var server_key = CONFIG.MERCHANT_FCM_SERVER_KEY;
                    var server_key = CONFIG.LOLOL_MERCHANT_FCM_SERVER_KEY;

                    MerchantDevice.findAll({attributes:['app_token','device_id'], where:where}).then(device =>{
                        fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Device Count: '+device.length +'\r\n');
                        
                        if(device.length > 0){
                            
                            var promises = [];
                            
                            var FCM = require('./lib/fcm');

                            var fcm = new FCM(server_key);

                            device.map(function(v) {
                                
                                function fcmPush(v){
                                    return new Promise((resolve, reject) => {
                                        
                                        fcmPushNotification(msg_payload,v.app_token,param, fcm).then(result =>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+result +'\r\n');
                                            
                                            resolve() 
                                        }).catch(err=>{
                                            fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ v.device_id+' '+err +'\r\n');
                                            resolve()
                                        }) 
                                    })
                                }
                                promises.push(fcmPush(v));
                            })
        
                            Promise.all(promises).then(function(result) {
                                
                                fs.appendFileSync(file, '['+moment().format('YYYY-MM-DD HH:mm:ss')+'] '+ action +' Notification End.\r\n');
                                resolve(); 
                                
                            }).catch(err => {
                                reject(err);
                            })
                        }
                        
                    }).catch(err =>{
                        reject(err)
                    })
                break;
            }

            
        }).catch(err =>{
            reject(err)
        })
    });
}
exports.handleNotification_lolol = handleNotification_lolol;


/**
* FCM Push Notification [Sends Push notification for Android users]
*
* @param  int  $registrationIds
* @param  int  $msg_body
* @param  int  $msg_title

* @return result
*/

var fcmPushNotification = function(data, reg_id, param = null, fcm) {
    return new Promise((resolve, reject) => {
        
        

        var fields = {
            to: reg_id,
            notification: {
                title: "",
                body: data['msg'],
                message: data['msg'],
                sound: "default"
            },
            priority: 'high'
        };

        if(param !== null)
            fields.data = param;

        return fcm.send(fields).then(function(response){
                
            resolve(response);
        }).catch(function(err){
                
            reject(err);
        })
    });
}
exports.fcmPushNotification = fcmPushNotification;

