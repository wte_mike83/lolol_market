
var getConfigSetting = function(){
    
    return new Promise((resolve, reject) => {
        AdminControl.findAll().then(admin_setting => {
            var setting = {};
            admin_setting.forEach(function(value) {
                setting[value.operation] = value.value;
        
            })
            resolve(setting);
        }).catch(function (err) {
            reject(err);
        })
    });
}
exports.getConfigSetting = getConfigSetting;

// var readFile = function(tempFiles){
//     const fs = require('fs');
//     return new Promise((resolve, reject) => {
//         if(!fs.existsSync(tempFiles)) {
//             reject(new Error('File Not Found!'));
//         }
//         resolve();
//     });
// }
// exports.readFile = readFile;

// var checkDirectorySync = function(directory){
//     const fs = require('fs');
//     return new Promise((resolve, reject) => {
//         fs.stat(directory, function(err) {
//             if(err)
//             {
//                  //Check if error defined and the error code is "not exists"
//                 if (err.code === 'ENOENT') {
//                     fs.mkdir(directory, (error) => {
//                         if (error) {
//                             if(err.code === 'ENOENT')
//                                 reject(new Error('Upload Diretory Not Found!'));
//                             else
//                                 reject(error);
//                         } else {
//                             resolve(directory);
//                         }
//                     });
//                 }else {
//                     //just in case there was a different error:            
//                     reject(err);
//                 }
//             } else {
//                 resolve(directory);
//             }
//         });
//     });
// }
// exports.checkDirectorySync = checkDirectorySync;

// var moveFile = function(oldPath, newPath){
//     const fs = require('fs');
//     return new Promise((resolve, reject) => {
//         fs.rename(oldPath, newPath, function (err) {
//             if(err)
//             {
//                 if (err.code === 'EXDEV') {
//                     copy();
//                 } else if (err.code === 'ENOENT') {
//                     reject(new Error('Diretory or File Not Found!'));
//                 }else {
//                     //just in case there was a different error:            
//                     reject(err);
//                 }
//             } else {
//                 resolve('complete');
//             }
//         });
//     });

//     function copy() {
//         var readStream = fs.createReadStream(oldPath);
//         var writeStream = fs.createWriteStream(newPath);

//         readStream.on('error', callback);
//         writeStream.on('error', callback);

//         readStream.on('close', function () {
//             fs.unlink(oldPath, callback);
//         });

//         readStream.pipe(writeStream);
//     }
// }
// exports.moveFile = moveFile;

// var getFilterKey = function(arr){
    
//     var Sequelize = require('sequelize');
//     const OtherFilter = require('../../../models').OtherFilter;

//     return new Promise((resolve, reject) => {
//         OtherFilter.findAll({attributes: ['filter_key', 'filter_id'], where:{type:'0', filter_id : {[Sequelize.Op.in] : arr}}}).then(ret => {
            
//             resolve(ret);
//         }).catch(function (err) {
//             reject(err);
//         })
//     });
// }
// exports.getFilterKey = getFilterKey;



// var generateRandomKey = function(length){

//     return new Promise((resolve, reject) => {
//         let start = 2;
//         let stop = parseInt(length) + start;

//         resolve(Math.random().toString(36).substring(start, stop));
//     });
  
// }
// exports.generateRandomKey = generateRandomKey;

var checkDirectorySync = function(directory){

    const fs = require('fs');
    return new Promise((resolve, reject) => {
        fs.stat(directory, function(err) {
            if(err)
            {
                 //Check if error defined and the error code is "not exists"
                if (err.code === 'ENOENT') {
                    console.log("directory"+directory)
                    fs.mkdir(directory, (error) => {
                        if (error) {
                            if(err.code === 'ENOENT')
                                reject(new Error('Upload Diretory Not Found!'+error));
                            else
                                reject(error);
                        } else {
                            resolve(directory);
                        }
                    });
                }else {
                    //just in case there was a different error:            
                    reject(err);
                }
            } else {
                resolve(directory);
            }
        });
    });
}
exports.checkDirectorySync = checkDirectorySync;